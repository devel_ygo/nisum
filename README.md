### NISUM
#### _Aplicación Backend en la cual podemos crear y consultar usaurios._
#### Requerimientos.
* Java 1.8.
* SpringBoot 2.7.
* Gradle 7.5.
* JUnit 5.
* H2
* Thymeleaf.

#### _El motor de base datos H2 requiere de los siguientes parametros:_
##### _Recursos_
 - Propiedades =`nisum-user/src/main/resources/application.properties`
 - Sritps SQL  =`nisum-user/src/main/resources/import.sql`
######
 - spring.datasource.url=jdbc:h2:mem:nisum_user;DATABASE_TO_UPPER=false;DB_CLOSE_ON_EXIT=FALSE
 - spring.datasource.driverClassName: org.h2.Driver
 - spring.jpa.defer-datasource-initialization=true
 - spring.datasource.username=nisum
 - spring.datasource.password=nisum
 - spring.h2.console.enabled=true
 - spring.jpa.show-sql=true
 - spring.jpa.hibernate.ddl-auto=create-drop
 - spring.jpa.properties.hibernate.format_sql=true
 - spring.h2.console.path=/h2
 - spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
#####
 - spring.mvc.pathmatch.matching-strategy=ant-path-matcher
 - spring.jackson.serialization.write-dates-as-timestamps=false
#### _Configuración parámetro de la expresión regular para validar la estructura de la contraseña._
 - Propiedades =`nisum-user/src/main/resources/parameters.properties`
 - Ejemplo: `param.pattern.password=^[0-9,$]*$`

#### _Puntos de acceso._
Este proyecto esta creado en springboot, por lo cual ya cuenta con un servidor por defecto en cual se puede desplegar
con solo iniciandolo desde un ide como SpringTools o Intellij IDEA desde la clase `NisumUserApplication.java` , o acceder desde
el dominio definido y posterior a su despliegue se puede acceder a los endpoints desde la url
`http://localhost:8080/api/user/`

1. *Se encuentra el recurso `/user/add/`, metodo POST que permite la estrctura JSON: ejemplo: `http://localhost:8080/api/user/add/`.*
    - Escenario cuando la estrucura de envio:
      `{
         "name": "Messi",
         "email": "messi@gana.com",
         "password": "2",
         "phones": [{
                  "number": "1234567",
                  "cityCode": "57",
                  "countryCode": "45"
                  }
                ]
      }`

    - Como respuesta satisfactorio será:
      `{
      "id": "1",
      "created": "2022-12-18",
      "modified": null,
      "lastLogin": "2022-12-18",
      "token": "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJOSVNVTSIsInN1YiI6IjIiLCJhdXRob3JpdHkiOiJrZXlTZWNyZXQiLCJpYXQiOjE2NzE0MTM4MzksImV4cCI6MTY3MTQxMzgzOX0.qBOM6RtL6FbsikJR5Z-_KJU-JllrAwqsVxO5gUymhnU",
      "isActive": "S"
      }`
   
    - Código de respuesta : 200
    - Descripción         : OK
    - Encabezados de respuesta:
      `connection: keep-alive  content-type: application/json  date: Mon19 Dec 2022 01:55:59 GMT  keep-alive: timeout=60  transfer-encoding: chunked `

    - Como respuesta incorrecto será:
      `{
        "message": "Formato de email incorrecto"
      }`
    - Código de respuesta : 400
    - Descripción         : Bad Request
    - Encabezados de respuesta:
      `connection: keep-alive  content-type: application/json  date: Mon19 Dec 2022 01:45:49 GMT  keep-alive: timeout=60  transfer-encoding: chunked`

    - también se puede intentar con el comando Curl
      `curl -X POST "http://localhost:8080/api/user/add/" -H  "accept: application/json" -H  "Content-Type: application/json" 
      -d "{"name": "Messi",
           "email":"messi@gana.com",    
           "password": "2",   
           "phones": [{"number": "1234567", 
                      "cityCode":"57",
                      "countryCode":"45"
                    }]
          }"`

2. *Se encuentra el recurso el metodo GET`/api/user/`.*
    - Parámetros de entrada: dato numérico que referencia al numero identificador del usuario registrado, se debería usar así:
      `http://localhost:8080/api/user/1`

    - Como respuesta satisfactorio será:
      `{
      "id": "1",
      "created": "2022-12-18",
      "modified": null,
      "lastLogin": "2022-12-18",
      "token": "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJOSVNVTSIsInN1YiI6IjIiLCJhdXRob3JpdHkiOiJrZXlTZWNyZXQiLCJpYXQiOjE2NzE0MTM4MzksImV4cCI6MTY3MTQxMzgzOX0.qBOM6RtL6FbsikJR5Z-_KJU-JllrAwqsVxO5gUymhnU",
      "isActive": "S"
      }`
    - Código de respuesta : 200 
    - Descripción         : OK
    - Encabezados de respuesta:
      `connection: keep-alive  content-type: application/json  date: Mon19 Dec 2022 01:45:49 GMT  keep-alive: timeout=60  transfer-encoding: chunked`

    - Como respuesta incorrecto será:
      `{
      "message": "No existe un usuario con Id 2"
      }`
    - Código de respuesta : 400 
    - Descripción         : Bad Request
    - Encabezados de respuesta:
      `connection: keep-alive  content-type: application/json  date: Mon19 Dec 2022 01:45:49 GMT  keep-alive: timeout=60  transfer-encoding: chunked`
   
    - también se puede intentar con el comando Curl
      `curl -X GET "http://localhost:8080/api/user/1" -H  "accept: application/json"`
- #### **_NOTA:_** Se puede obtener mas información en la url de Swagger:
    * Información Xml `http://localhost:8080/v2/api-docs`
    * Información grafica `http://localhost:8080/swagger-ui/`
