package com.api.nisum.model.entity;

import com.api.nisum.ExceptionGeneral;
import com.api.nisum.exceptions.BusinessException;
import com.api.nisum.exceptions.ExceptionObligateValue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class UserTest extends ExceptionGeneral {
    private static final String EL_CAMPO_S_ES_OBLIGATORIO = "El campo %s es obligatorio";
    private static final String FORMATO_DE_CADENA_INCORRECTO = "Formato de %s incorrecto";
    public static final String NAME = "karol";
    public static final String EMAIL = "karol@gmail.com";
    public static final String PASSWORD = "1";
    private User user;
    @BeforeEach
    void setUp() {
        user = new User();
    }

    @Test
    public void createUserOkTest (){
        // Act
        user = new User(NAME, EMAIL, PASSWORD);
        // Assert
        assertNotNull(user);
        assertEquals(user.getName(),NAME );
        assertEquals(user.getEmail(),EMAIL );
        assertEquals(user.getPassword(),PASSWORD);
        assertEquals(user.getIsActive(),"S");
        assertEquals(user.getCreated(), LocalDate.now());
        assertNull(user.getModified());
    }

    @Test
    public void whenNameIsNullThrowExceptionTest() {
        setaThrows(Assertions.assertThrows(ExceptionObligateValue.class, () -> {
            user = new User(null, EMAIL, PASSWORD);
        }));
        assertEquals(getaThrows().getMessage(), String.format(EL_CAMPO_S_ES_OBLIGATORIO, "name"));
    }
    @Test
    public void whenEmailIsNullThrowExceptionTest() {
        setaThrows(Assertions.assertThrows(ExceptionObligateValue.class, () -> {
            user = new User(NAME, null, PASSWORD);
        }));
        assertEquals(getaThrows().getMessage(), String.format(EL_CAMPO_S_ES_OBLIGATORIO, "email"));
    }

    @Test
    public void whenPasswordIsNullThrowExceptionTest() {
        setaThrows(Assertions.assertThrows(ExceptionObligateValue.class, () -> {
            user = new User(NAME, EMAIL, null);
        }));
        assertEquals(getaThrows().getMessage(), String.format(EL_CAMPO_S_ES_OBLIGATORIO, "password"));
    }

    @Test
    public void whenIncorrectFormatEmailThrowExceptionTest() {
        setaThrows(Assertions.assertThrows(BusinessException.class, () -> {
            user = new User(NAME, "a.a@a.0m", PASSWORD);
        }));
        assertEquals(getaThrows().getMessage(), String.format(FORMATO_DE_CADENA_INCORRECTO, "email"));
    }

    @AfterEach
    public void afterUp() {
        user = null;
    }
}