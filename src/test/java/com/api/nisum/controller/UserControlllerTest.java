package com.api.nisum.controller;

import com.api.nisum.builder.PhoneUserInDataDtoBuilder;
import com.api.nisum.builder.UserOutDataDtoBuilder;
import com.api.nisum.exceptions.BusinessException;
import com.api.nisum.model.dto.UserInDataDto;
import com.api.nisum.model.dto.UserOutDataDto;
import com.api.nisum.services.impl.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;
import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserControlller.class)
class UserControlllerTest {
    public static final String PATH_ENDPOINT_POST = "/api/user/add/";
    public static final String PATH_ENDPOINT_GET = "/api/user/1";
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService services;
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void whenFindUserByIdTest() throws Exception {
        // Arrange
        final UserInDataDto userInDataDto = new UserInDataDto();
        userInDataDto.setName("Yefri");
        userInDataDto.setEmail("yefry.g@gmail.com");
        userInDataDto.setPassword("1");
        userInDataDto.setPhones(Arrays.asList(new PhoneUserInDataDtoBuilder().build()));

        final UserOutDataDto outDataDto = new UserOutDataDtoBuilder().withId("1").build();

        when(services.findById(anyLong())).thenReturn(outDataDto);
        // Act
        mockMvc.perform(get(PATH_ENDPOINT_GET).contentType(MediaType.APPLICATION_JSON))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.created").value(LocalDate.now().toString()))
        ;
        verify(services).findById(1L);
    }

    @Test
    void whenAdduserTest() throws Exception {
        // Arrange
        final UserInDataDto userInDataDto = new UserInDataDto();
        userInDataDto.setName("Yefri");
        userInDataDto.setEmail("yefry.g@gmail.com");
        userInDataDto.setPassword("1");
        userInDataDto.setPhones(Arrays.asList(new PhoneUserInDataDtoBuilder().build()));

        final UserOutDataDto outDataDto = new UserOutDataDtoBuilder().withId("1")
                .withCreated(LocalDate.now())
                .withModified(LocalDate.now())
                .withLastLogin(LocalDate.now())
                .withIsActive("s")
                .build();

        when(services.create(any())).thenReturn(outDataDto);
        // Act
        ResultActions response = mockMvc.perform(post(PATH_ENDPOINT_POST)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userInDataDto)));
        // Assert
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.created").value(LocalDate.now().toString()))
                .andReturn()
                .getResponse()
                .getContentAsString();

        verify(services).create(any());

    }
    @Test
    void whenFormatEmailIncorrectTest() throws Exception {
        // Arrange
        final UserInDataDto userInDataDto = new UserInDataDto();
        userInDataDto.setName("Yefri");
        userInDataDto.setEmail("yefry.g@gmail.c1om");
        userInDataDto.setPassword("1");
        userInDataDto.setPhones(Arrays.asList(new PhoneUserInDataDtoBuilder().build()));

        final UserOutDataDto outDataDto = new UserOutDataDtoBuilder().withId("1")
                .withCreated(LocalDate.now())
                .withModified(LocalDate.now())
                .withLastLogin(LocalDate.now())
                .withIsActive("s")
                .build();

        doThrow(new BusinessException("Formato de email incorrecto")).when(services).create(any());
        // Act
        ResultActions response = mockMvc.perform(post(PATH_ENDPOINT_POST)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userInDataDto)));
        // Assert
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Formato de email incorrecto"))
                .andReturn()
                .getResponse()
                .getContentAsString();

        verify(services).create(any());

    }

    @AfterEach
    void tearDown() {
    }
}