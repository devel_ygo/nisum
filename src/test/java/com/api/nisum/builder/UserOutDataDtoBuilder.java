package com.api.nisum.builder;

import com.api.nisum.model.dto.UserOutDataDto;

import java.io.Serializable;
import java.time.LocalDate;

public class UserOutDataDtoBuilder implements Serializable {
	private String id;
    private LocalDate created;
    private LocalDate modified;
    private LocalDate lastLogin;
    private String token;
    private String isActive;

    public UserOutDataDtoBuilder() {
        this.id = "0";
        this.created = LocalDate.now();
        this.modified = LocalDate.now();
        this.lastLogin = LocalDate.now();
        this.token = null;
        this.isActive = null;
    }

    public UserOutDataDtoBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public UserOutDataDtoBuilder withCreated(LocalDate created) {
        this.created = created;
        return this;
    }

    public UserOutDataDtoBuilder withModified(LocalDate modified) {
        this.modified = modified;
        return this;
    }

    public UserOutDataDtoBuilder withLastLogin(LocalDate lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public UserOutDataDtoBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public UserOutDataDtoBuilder withIsActive(String isActive) {
        this.isActive = isActive;
        return this;
    }

    public UserOutDataDto build (){
        return new UserOutDataDto(id, created,modified,lastLogin,token,isActive);
    }
}
