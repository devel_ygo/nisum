package com.api.nisum.builder;

import com.api.nisum.model.dto.PhoneUserInDataDto;

public class PhoneUserInDataDtoBuilder {
    private String number;
    private String cityCode;
    private String countryCode;

    public PhoneUserInDataDtoBuilder() {
        number = "1";
        cityCode = "00";
        countryCode = "00";
    }

    public PhoneUserInDataDtoBuilder setNumber(String number) {
        this.number = number;
        return this;
    }

    public PhoneUserInDataDtoBuilder setCityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public PhoneUserInDataDtoBuilder setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public PhoneUserInDataDto build (){
        return new PhoneUserInDataDto(number,cityCode,countryCode);
    }
}
