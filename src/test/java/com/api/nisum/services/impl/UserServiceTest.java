package com.api.nisum.services.impl;

import com.api.nisum.ExceptionGeneral;
import com.api.nisum.builder.PhoneUserInDataDtoBuilder;
import com.api.nisum.exceptions.BusinessException;
import com.api.nisum.exceptions.ValidateArgument;
import com.api.nisum.model.dto.PhoneUserInDataDto;
import com.api.nisum.model.dto.UserInDataDto;
import com.api.nisum.model.dto.UserOutDataDto;
import com.api.nisum.model.entity.PhoneUser;
import com.api.nisum.model.entity.User;
import com.api.nisum.repository.PhoneUserRepository;
import com.api.nisum.repository.UserRepository;
import com.api.nisum.security.ConfigSecurity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest extends ExceptionGeneral {
    public static final String NAME = "karol";
    public static final String EMAIL = "karol@gmail.com";
    public static final String PASSWORD = "1";
    public static final String EL_CORREO_YA_ESTA_REGISTRADO = "El correo "+EMAIL+" ya esta registrado.";
    @Mock
    private UserRepository userRepository;
    @Mock
    private PhoneUserRepository phoneUserRepository;
    @Spy
    private ModelMapper modelMapper;
    @Spy
    private ConfigSecurity security;
    @Spy
    private ValidateArgument validateArgument;
    @InjectMocks
    private UserService userService;

    private User user;
    private PhoneUser phoneUser;
    private UserOutDataDto dto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new User(NAME, EMAIL, PASSWORD);
        phoneUser =new PhoneUser();
    }

    @Test
    void findByIdOkTest() {
        // Arrange
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        // Act
        dto = userService.findById(anyLong());
        // Assert
        assertNotNull(dto);
        assertEquals(dto.getCreated(), LocalDate.now());
    }

    @Test
    public void whenFindByIdUserNoExistTest (){
        setaThrows(Assertions.assertThrows(BusinessException.class, () -> {
            dto = userService.findById(anyLong());
        }));
        assertNotNull(getaThrows());
        assertTrue(getaThrows().getMessage().contains("No existe un usuario"));
    }

    @Test
    void createOkTest() {
        // Arrange
        UserInDataDto dtoIn = new UserInDataDto();
        dtoIn.setName(NAME);
        dtoIn.setEmail(EMAIL);
        dtoIn.setPassword(PASSWORD);
        PhoneUserInDataDto inDataDto = new PhoneUserInDataDtoBuilder().build();

        dtoIn.setPhones(Arrays.asList(inDataDto));

        doReturn(Optional.empty()).when(userRepository).findByEmail(anyString());

        when(userRepository.save(any())).thenReturn(user);
        when(phoneUserRepository.save(any())).thenReturn(phoneUser);
        // Act
        dto = userService.create(dtoIn);
        // Assert
        verify(userRepository).save(any());
        verify(phoneUserRepository,atLeastOnce()).save(any());
        assertNotNull(dto);
        assertNotNull(dto.getCreated());
        assertNotNull(dto.getLastLogin());
        assertNull(dto.getModified());
    }

    @Test
    public void whenExistEmailThrowExceptionTest (){
        // Arrange
        UserInDataDto dtoIn = new UserInDataDto();
        dtoIn.setName(NAME);
        dtoIn.setEmail(EMAIL);
        dtoIn.setPassword(PASSWORD);

        doReturn(Optional.of(user)).when(userRepository).findByEmail(anyString());
        // Act
        setaThrows(Assertions.assertThrows(BusinessException.class, () -> dto = userService.create(dtoIn)));
        // Assert
        verify(userRepository,atLeastOnce()).findByEmail(any());
        assertNotNull(getaThrows());
        assertEquals(getaThrows().getMessage(), EL_CORREO_YA_ESTA_REGISTRADO);
    }
    @AfterEach
    public void afterUp() {
        dto = null;
        user = null;
    }

}