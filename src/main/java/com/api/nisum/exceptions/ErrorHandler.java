package com.api.nisum.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER_ERROR = LoggerFactory.getLogger(ErrorHandler.class);

    private static final String OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR = "Ocurrió un error favor contactar al administrador.";

    private static final ConcurrentHashMap<String, Integer> CODIGOS_ESTADO = new ConcurrentHashMap<>();

    public ErrorHandler (){
        CODIGOS_ESTADO.put(IllegalArgumentException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODIGOS_ESTADO.put(ExceptionObligateValue.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODIGOS_ESTADO.put(BusinessException.class.getSimpleName(),HttpStatus.BAD_REQUEST.value());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception exception, HttpServletRequest response) {
        ResponseEntity<Error> result;

        String name = exception.getClass().getSimpleName();
        String message = exception.getMessage();
        Integer code = CODIGOS_ESTADO.get(name);
        System.out.println(code);

        if (code != null) {
            ErrorDto error = new ErrorDto(message);
            result = new ResponseEntity(error, HttpStatus.valueOf(code));
        } else {
            LOGGER_ERROR.error(message, exception);
            ErrorDto error = new ErrorDto(OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR);
            result = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return result;
    }
}
