package com.api.nisum.exceptions;

public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NotFoundException(String text) {
        super(text);
    }
}