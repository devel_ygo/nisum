package com.api.nisum.controller;

import com.api.nisum.exceptions.BusinessException;
import com.api.nisum.model.dto.UserInDataDto;
import com.api.nisum.model.dto.UserOutDataDto;
import com.api.nisum.services.IUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/api/user")
@Api(value = "Registro de usuario nisum", description = "Api Rest que permitira registrar usuario para nisum")
public class UserControlller {

    @Autowired
    private IUser iUser;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Numero de registro del usuario", notes = "Retorna el usuario encontrado de acuerdo al Id dado." )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. Si el usuario es encontrado.", response = Map.class ),
            @ApiResponse(code = 400, message = "Bad Request. En caso de que el usuario no sea encontrado", response = Map.class),})
    public ResponseEntity<?> find(@PathVariable(required = true) Long id) throws BusinessException {
        System.out.println("id "+id);
        UserOutDataDto outDataDto = iUser.findById(id);
        System.err.println(outDataDto.toString());
        return ResponseEntity.ok(outDataDto);
    }

    @RequestMapping(value = "/add/",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "JSON con los parametros necesarios para el registro del usuario", notes = "Dado un JSON con la estructura predeterminada se registrara el usuario" )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Created. Si el usuario es registrado correctamente.", response = Map.class ),
            @ApiResponse(code = 400, message = "Bad Request. En caso de que el usuario no sea registrado.", response = Map.class),})
    public UserOutDataDto addUser (@RequestBody UserInDataDto user) throws BusinessException {
        return  iUser.create(user);
    }
}
