package com.api.nisum.repository;

import com.api.nisum.model.entity.PhoneUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneUserRepository extends JpaRepository<PhoneUser, Long> {
	
	@Query(value = "SELECT * FROM phone_users WHERE id_user = ?1", nativeQuery = true)
	List<PhoneUser> findByIdUser(Long id_user);
}
