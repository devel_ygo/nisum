package com.api.nisum.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8920382030433063051L;
	private String name;
    private String email;
}
