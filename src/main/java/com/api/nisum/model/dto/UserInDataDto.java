package com.api.nisum.model.dto;

import com.api.nisum.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInDataDto implements Serializable {
	private static final long serialVersionUID = 8324665584926481163L;
	private String name;
    private String email;
    private String password;
    private List<PhoneUserInDataDto> phones;
    
    public User mapUserDtoToUser (UserInDataDto dataDto) {
    	return new User(dataDto.getName(), dataDto.getEmail(), dataDto.getPassword());
    }
}
