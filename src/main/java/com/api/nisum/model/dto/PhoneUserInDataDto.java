package com.api.nisum.model.dto;

import com.api.nisum.model.entity.PhoneUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneUserInDataDto implements Serializable {

	private static final long serialVersionUID = -590068060524266398L;
	private String number;
    private String cityCode;
    private String countryCode;

    public List<PhoneUser> mapPhoneUserDtoToPhoneUser (List<PhoneUserInDataDto> dataDto) {
        return dataDto.stream().map(phoneUser -> {        	
        	return new PhoneUser(phoneUser.getNumber(), phoneUser.getCityCode(), phoneUser.getCountryCode());
        }).collect(Collectors.toList());
    }
}
