package com.api.nisum.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOutDataDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5325029211099498457L;
	private String id;
    private LocalDate created;
    private LocalDate modified;
    private LocalDate lastLogin;
    private String token;
    private String isActive;
}
