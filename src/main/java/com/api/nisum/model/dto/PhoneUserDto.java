package com.api.nisum.model.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class PhoneUserDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private long number;
	private int cityCode;
	private int contryCode;
}
