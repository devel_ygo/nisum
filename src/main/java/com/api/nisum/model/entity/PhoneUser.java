package com.api.nisum.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "phone_users")
public class PhoneUser implements Serializable {

	private static final long serialVersionUID = -3973725685025622929L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;    
    @Column(length = 25, nullable = false)
    private String number;
    @Column(name = "city_code")
    private String cityCode;
    @Column(name = "country_code")
    private String countryCode;

    @ManyToOne (fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="id_user")
    @JsonIgnore
    private User user;
    
	public PhoneUser(String number, String cityCode, String countryCode) {
		this.number = number;
		this.cityCode = cityCode;
		this.countryCode = countryCode;
	}

}