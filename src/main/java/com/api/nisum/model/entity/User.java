package com.api.nisum.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import static com.api.nisum.exceptions.ValidateArgument.obligateValue;
import static com.api.nisum.exceptions.ValidateArgument.valideteFormat;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 883343709722488286L;
	private static final String EL_CAMPO_S_ES_OBLIGATORIO = "El campo %s es obligatorio";
	private static final String FORMATO_DE_CADENA_INCORRETO = "Formato de %s incorrecto";
	private static final String PATTERN_EMAIL = "[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,5}";
	private static final String PATTERN_PASSWORD = "[a-zA-Z0-9]";

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String name;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private LocalDate created;
    @Column
    private LocalDate modified;
    @Column
    private LocalDate lastLogin;
    @Column
    private String token;
    @Column
    private String isActive;
    @OneToMany(mappedBy = "user")
    private List<PhoneUser> phoneUsers;

	public User(String name, String email, String password) {
		obligateValue(name, String.format(EL_CAMPO_S_ES_OBLIGATORIO, "name"));
		obligateValue(email, String.format(EL_CAMPO_S_ES_OBLIGATORIO, "email"));
		valideteFormat(email, PATTERN_EMAIL, String.format(FORMATO_DE_CADENA_INCORRETO,"email"));
		obligateValue(password, String.format(EL_CAMPO_S_ES_OBLIGATORIO, "password"));

		this.name = name;
		this.email = email;
		this.password = password;
        this.isActive = "S";
		this.created = LocalDate.now();
        this.lastLogin = LocalDate.now();
	}
	
	public void setPhoneUsers (List<PhoneUser> phoneUsers) {
		this.phoneUsers = phoneUsers;
	}

    public void setModified (){
        this.modified = LocalDate.now();
    }
    public void setlastLogin (){
        this.lastLogin = LocalDate.now();
    }
    
}
