package com.api.nisum.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ConfigSecurity {

    public static final String ID = "NISUM";
    public static final String AUTHORITY = "authority";
    public static final String KEY_SECRET = "keySecret";
    public static final String ROLE_USER = "ROLE_USER";

    public String getToken (String key){
        String token;

        List<GrantedAuthority> authority = AuthorityUtils.commaSeparatedStringToAuthorityList(ROLE_USER);

        token = Jwts.builder()
                .setIssuer(ID)
                .setSubject(key)
                .claim(AUTHORITY, KEY_SECRET)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.HS256, KEY_SECRET.getBytes()
                ).compact();
        return token;
    }
}

