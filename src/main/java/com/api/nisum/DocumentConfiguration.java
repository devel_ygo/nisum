package com.api.nisum;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class DocumentConfiguration {
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.api.nisum.controller"))
                .paths(PathSelectors.any()).build();
    }

    @SuppressWarnings("unused")
	private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Nisum Api's")
                .description ("Api que permite registrar usuario para NISUM!")
                .version("1.0")
                .build();
    }
}
