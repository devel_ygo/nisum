package com.api.nisum.services;

import com.api.nisum.generic.IGenericDao;
import com.api.nisum.model.dto.UserInDataDto;
import com.api.nisum.model.dto.UserOutDataDto;

public interface IUser extends IGenericDao<UserInDataDto, UserOutDataDto> {
}
