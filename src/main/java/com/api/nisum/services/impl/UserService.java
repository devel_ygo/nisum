package com.api.nisum.services.impl;

import com.api.nisum.exceptions.BusinessException;
import com.api.nisum.model.dto.UserInDataDto;
import com.api.nisum.model.dto.UserOutDataDto;
import com.api.nisum.model.entity.PhoneUser;
import com.api.nisum.model.entity.User;
import com.api.nisum.repository.PhoneUserRepository;
import com.api.nisum.repository.UserRepository;
import com.api.nisum.security.ConfigSecurity;
import com.api.nisum.services.IUser;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

import static com.api.nisum.exceptions.ValidateArgument.valideteFormat;

@Service
@PropertySource("classpath:parameters.properties")
public class UserService implements IUser {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    public static final String EL_CORREO_S_YA_ESTA_REGISTRADO = "El correo %s ya esta registrado.";
    public static final String NO_EXISTE_UN_USUARIO_CON_ID_S = "No existe un usuario con Id %s";

    @Value("${param.pattern.password:^[0-9,$]*$}")
    private String PATTERN_VALIDATE_INPUT_PASSWORD;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhoneUserRepository phoneUserRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ConfigSecurity security;

    @Override
    public UserOutDataDto findById(Long id) {
        System.out.println(id);
        User user = userRepository.findById(id).orElse(null);
        if (Objects.isNull(user)) {
            throw new BusinessException(String.format(NO_EXISTE_UN_USUARIO_CON_ID_S,id));
        }
        return modelMapper.map(user, UserOutDataDto.class);
    }

    @Override
    public UserOutDataDto create(UserInDataDto userInDataDto) {
 
    	if (Objects.nonNull(userInDataDto) || !userInDataDto.getPhones().isEmpty()) {
            validEmailExist(userInDataDto.getEmail());
        }

        if (Objects.nonNull(PATTERN_VALIDATE_INPUT_PASSWORD) && !userInDataDto.getPassword().matches(PATTERN_VALIDATE_INPUT_PASSWORD)){
            throw new BusinessException(String.format("La contrasena %s no cumple con el formato %s.",userInDataDto.getPassword(), PATTERN_VALIDATE_INPUT_PASSWORD));
        }


        User user = userInDataDto.mapUserDtoToUser(userInDataDto);
        user.setToken(security.getToken(user.getPassword()));
        user = userRepository.save(user);

        user.setPhoneUsers(userInDataDto.getPhones().stream().map(pu -> {
            return new PhoneUser(pu.getNumber(), pu.getCityCode(), pu.getCountryCode());
        }).collect(Collectors.toList()));

		for (PhoneUser phoneUser : user.getPhoneUsers()) {
            LOGGER.info("user {} number {} cityCode {} countryCode {}",user.getId(), phoneUser.getNumber(), phoneUser.getCityCode(), phoneUser.getCountryCode());
            phoneUser.setUser(user);
			phoneUserRepository.save(phoneUser);
		}        
        return modelMapper.map(user, UserOutDataDto.class);
    }

	private void validEmailExist(String email) {
        User user = userRepository.findByEmail(email).orElse(null);
		if ( user != null ) {
            LOGGER.warn("El correo {} ya esta registrado",email);
		    throw new BusinessException(String.format(EL_CORREO_S_YA_ESTA_REGISTRADO,email));
		}
	}
}
