package com.api.nisum.generic;

import com.api.nisum.exceptions.BusinessException;

public interface IGenericDao <T, R> {
    R findById(Long id);
    R create(T t) throws BusinessException;
}